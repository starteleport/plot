﻿
Public Class CollectionEventArgs
    Inherits System.EventArgs

    Private m_NewValue As Object
    Public Property NewValue() As Object
        Get
            Return m_NewValue
        End Get
        Set(ByVal value As Object)
            m_NewValue = value
        End Set
    End Property

    Private m_OldValue As Object
    Public Property OldValue() As Object
        Get
            Return m_OldValue
        End Get
        Set(ByVal value As Object)
            m_OldValue = value
        End Set
    End Property

    Private m_Index As Integer
    Public Property Index() As Integer
        Get
            Return m_Index
        End Get
        Set(ByVal value As Integer)
            m_Index = value
        End Set
    End Property

    Public Sub New()
        MyBase.New()

        m_OldValue = Nothing
        m_NewValue = Nothing
        m_Index = -1
    End Sub

    Public Sub New(ByVal Index As Integer, ByVal NewValue As Object, Optional ByVal OldValue As Object = Nothing)
        MyBase.New()

        m_OldValue = OldValue
        m_NewValue = NewValue
        m_Index = Index
    End Sub

End Class

