﻿Imports System.Drawing

Public Class LineStrip
    Inherits System.Collections.Generic.List(Of PointF)

    Private m_Color As System.Drawing.Color
    Public Property Color() As System.Drawing.Color
        Get
            Return m_Color
        End Get
        Set(ByVal value As System.Drawing.Color)
            m_Color = value
        End Set
    End Property

    Private m_Visible As Boolean
    Public Property Visible() As Boolean
        Get
            Return m_Visible
        End Get
        Set(ByVal value As Boolean)
            m_Visible = value
        End Set
    End Property

#Region " Конструкторы "
    Public Sub New()
        m_Color = Drawing.Color.Red 'Чтобы свойство красиво сворачивалось :)
        m_Visible = True
    End Sub

    Public Sub New(ByVal Color As System.Drawing.Color)
        m_Color = Color
        m_Visible = True
    End Sub

    Public Property Tag() As Integer
        Get

        End Get
        Set(ByVal value As Integer)

        End Set
    End Property
#End Region

End Class