﻿Imports System.Windows.Forms

Public Class MouseEventArgsExtended
    Inherits MouseEventArgs

    Private m_tX As Single
    Public ReadOnly Property tX() As Single
        Get
            Return m_tX
        End Get
    End Property

    Private m_tY As Single
    Public ReadOnly Property tY() As Single
        Get
            Return m_tY
        End Get
    End Property

    Public Sub New( _
        ByVal button As MouseButtons, _
        ByVal clicks As Integer, _
        ByVal x As Integer, _
        ByVal y As Integer, _
        ByVal delta As Integer _
    )
        MyBase.New(button, clicks, x, y, delta)

    End Sub

    Public Sub New( _
        ByVal button As MouseButtons, _
        ByVal clicks As Integer, _
        ByVal x As Integer, _
        ByVal y As Integer, _
        ByVal delta As Integer, _
        ByVal tX As Single, _
        ByVal tY As Single _
    )
        MyBase.New(button, clicks, x, y, delta)

        m_tY = tY
        m_tX = tX

    End Sub

    Public Sub New( _
    ByVal EventArg As MouseEventArgs, _
    ByVal tX As Single, _
    ByVal tY As Single _
)
        MyBase.New(EventArg.Button, EventArg.Clicks, EventArg.X, EventArg.Y, EventArg.Delta)

        m_tY = tY
        m_tX = tX

    End Sub
End Class