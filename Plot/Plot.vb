Option Explicit On
Imports System.Drawing
Imports System.Collections.Generic
Imports System.Windows.Forms

Public Class Plot
    Inherits Windows.Forms.UserControl

    Private m_fontSizePixels As Single

    Private m_pixel As Single
    Private m_center As New Point
    Private m_pixelsInUnitY As Single
    Private m_pixelsInUnitX As Single
    Private m_ySize As Single
    Private m_xSize As Single

    Private m_CachedImage As Bitmap

    Public WithEvents Objects As New LineStrips

    Dim m_editedLineStrip As LineStrip
    Private m_EditMode As Boolean

    Public ReadOnly Property EditMode() As Boolean
        Get
            Return m_EditMode
        End Get
    End Property

    Public ReadOnly Property Center() As Point
        Get
            Return m_center
        End Get
    End Property

    Public ReadOnly Property Pixel() As Single
        Get
            Return m_pixel
        End Get
    End Property

    Private m_xMin As Single
    Public Property xMin() As Single
        Get
            Return m_xMin
        End Get
        Set(ByVal value As Single)
            m_xMin = value
            CalculateValues()
        End Set
    End Property

    Private m_yMin As Single

    Public Property yMin() As Single
        Get
            Return m_yMin
        End Get
        Set(ByVal value As Single)
            m_yMin = value
            CalculateValues()
        End Set
    End Property

    Private m_xMax As Single
    Public Property xMax() As Single
        Get
            Return m_xMax
        End Get
        Set(ByVal value As Single)
            m_xMax = value
            CalculateValues()
        End Set
    End Property

    Private m_yMax As Single

    Private m_LockRepaint As Boolean
    Public Property LockRepaintByCollectionEvents() As Boolean
        Get
            Return m_LockRepaint
        End Get
        Set(ByVal value As Boolean)
            m_LockRepaint = value
        End Set
    End Property

    Public Property yMax() As Single
        Get
            Return m_yMax
        End Get
        Set(ByVal value As Single)
            m_yMax = value
            CalculateValues()
        End Set
    End Property

    Protected Overrides Sub OnResize(ByVal e As System.EventArgs)
        CalculateValues()
        MyBase.OnResize(e)
    End Sub

    Protected Overrides Sub InitLayout()
        CalculateValues()
        MyBase.InitLayout()
    End Sub

    Private Sub CalculateValues()
        If Not m_CachedImage Is Nothing Then m_CachedImage.Dispose()
        m_CachedImage = Nothing

        m_xSize = m_xMax - m_xMin
        m_ySize = m_yMax - m_yMin

        If m_xSize <> 0 And m_ySize <> 0 Then
            m_pixelsInUnitX = (Width) / (1.1 * m_xSize)
            m_fontSizePixels = Math.Max(10.0F, Math.Min(Height / 30.0F, 15.0F))
            m_pixelsInUnitY = (Height - m_fontSizePixels) / (1.1 * m_ySize)
            m_pixel = Math.Min(1 / m_pixelsInUnitX, 1 / m_pixelsInUnitY)

            m_center.X = CInt(Math.Abs(m_xMin - 0.05 * m_xSize) * m_pixelsInUnitX)
            m_center.Y = CInt(Math.Abs(m_yMax + 0.05 * m_ySize) * m_pixelsInUnitY)

            m_CachedImage = New Bitmap(Width, Height)
            InvalidateCache()

        End If
    End Sub

    Private Sub InvalidateCache()
        Using g As Graphics = Graphics.FromImage(m_CachedImage)
            DrawCoordinateSystem(g)
            DrawObjects(g)
        End Using
    End Sub

#Region " Рисование "
    Protected Sub DrawCoordinateSystem(ByVal g As Graphics)
        Const TickSize As Integer = 4

        g.Clear(Me.BackColor)

        ' Определяем положение начала координат
        g.TranslateTransform(m_center.X, m_center.Y)
        g.ScaleTransform(m_pixelsInUnitX, -m_pixelsInUnitY)

        Dim pencil As New Pen(Color.Black, m_pixel)

        g.DrawLine(pencil, m_xMin - 0.05F * m_xSize, 0, m_xMax + 0.05F * m_xSize, 0)
        g.DrawLine(pencil, 0, m_yMin - 0.05F * m_ySize, 0, m_yMax + 0.05F * m_ySize)

        ' Насечки по целым точкам
        g.ScaleTransform(1, -1)

        ' По Х
        For i As Integer = Fix(m_xMin) To Fix(m_xMax)
            If i = 0 Then Continue For
            g.DrawLine(pencil, i, TickSize * m_pixel, i, -TickSize * m_pixel)
        Next i

        For i As Integer = Fix(m_yMin) To Fix(m_yMax)
            If i = 0 Then Continue For
            g.DrawLine(pencil, -TickSize * m_pixel, -i, TickSize * m_pixel, -i)
        Next i

        g.ResetTransform()

        Dim fontFamily As New FontFamily("Arial")
        Dim font As Font = New Font(fontFamily, m_fontSizePixels, FontStyle.Regular, GraphicsUnit.Pixel)

        'Подписи по Х
        Dim stepX As Integer = IIf((Fix(m_xMax) - Fix(m_xMin)) * m_fontSizePixels > Width * 0.45, Math.Round(2 * (Fix(m_xMax) - Fix(m_xMin)) / CInt((Width * 0.7) / (m_fontSizePixels))), 1)
        If stepX = 0 Then stepX = 1

        For tickPointValue As Integer = Fix(m_xMin) To Fix(m_xMax) Step stepX
            'If i = 0 Then Continue For
            g.DrawString(tickPointValue, font, Brushes.Black, m_center.X - m_fontSizePixels / 3 + (tickPointValue) * m_pixelsInUnitX, m_center.Y + 0.6 * m_fontSizePixels)
        Next tickPointValue

        Dim stepY As Integer = IIf((Fix(m_yMax) - Fix(m_yMin)) * m_fontSizePixels > Height * 0.45, Math.Round(2 * (Fix(m_yMax) - Fix(m_yMin)) / CInt((Height * 0.7) / (m_fontSizePixels))), 1)
        If stepY = 0 Then stepY = 1

        For tickPointValue As Integer = Fix(m_yMin) To Fix(m_yMax) Step stepY
            If tickPointValue = 0 Then Continue For
            g.DrawString(tickPointValue, font, Brushes.Black, m_center.X + m_fontSizePixels / 2, m_center.Y - (tickPointValue) * m_pixelsInUnitY - Math.Min(m_fontSizePixels / 2, m_pixelsInUnitY))
        Next tickPointValue

        'g.DrawString(0, f, Brushes.Black, center.X + 0.2 * pixelsInUnitX, center.Y - 0.5 * pixelsInUnitY)
    End Sub

    Protected Sub DrawObjects(ByVal g As Graphics)
        Dim pencil As New Pen(Color.Red, m_pixel)

        g.TranslateTransform(m_center.X, m_center.Y)
        g.ScaleTransform(m_pixelsInUnitX, -m_pixelsInUnitY)

        For Each o As LineStrip In Objects
            If o.Visible Then
                pencil.Color = o.Color
                Dim k As Integer
                For i As Integer = 0 To o.Count - 2
                    k = 1
                    While i + k < (o.Count - 1) AndAlso (Math.Sqrt(((o(i).X - o(i + k).X) ^ 2 + (o(i).Y - o(i + k).Y) ^ 2)) < m_pixel)
                        k += 1
                    End While

                    g.DrawLine(pencil, o(i).X, o(i).Y, o(i + k).X, o(i + k).Y)
                    i += k - 1
                Next i
            End If
        Next

    End Sub

    Protected Sub DrawObject(ByVal what As LineStrip)
        Using g As Graphics = Me.CreateGraphics
            Dim pencil As New Pen(what.Color, m_pixel)

            g.TranslateTransform(m_center.X, m_center.Y)
            g.ScaleTransform(m_pixelsInUnitX, -m_pixelsInUnitY)

            For i As Integer = 0 To what.Count - 2
                g.DrawLine(pencil, what(i).X, what(i).Y, what(i + 1).X, what(i + 1).Y)
            Next i
        End Using
    End Sub

    Protected Overrides Sub OnPaint(ByVal e As System.Windows.Forms.PaintEventArgs)
        If Not m_CachedImage Is Nothing Then
            e.Graphics.DrawImage(m_CachedImage, 0, 0)
            If m_EditMode Then DrawObject(m_editedLineStrip)
        End If

        MyBase.OnPaint(e)
    End Sub

    
    Public Sub PaintMe(ByVal ForceFullRepaint As Boolean)
        If ForceFullRepaint Then InvalidateCache()

        Me.Refresh()
    End Sub
#End Region

#Region " События коллекции "

    Private Sub Objects_Cleared(ByVal sender As Object, ByVal e As CollectionEventArgs) Handles Objects.Cleared
        InvalidateCache()
        Me.Refresh()
    End Sub

    Private Sub Objects_Inserting(ByVal sender As Object, ByVal e As CollectionEventArgs) Handles Objects.Inserting
        'Dim tmp_gr As Graphics = Graphics.FromImage(m_CachedImage)

        'DrawObject(e.NewValue, tmp_gr)
        'tmp_gr.Dispose()
        'tmp_gr = Nothing

        'tmp_gr = Me.CreateGraphics
        'tmp_gr.DrawImage(m_CachedImage, 0, 0)
        'tmp_gr.Dispose()
        If Not m_LockRepaint Then
            InvalidateCache()
            Me.Refresh()
        End If
    End Sub

    Private Sub Objects_Removing(ByVal sender As Object, ByVal e As CollectionEventArgs) Handles Objects.Removing
        If Not m_LockRepaint Then
            InvalidateCache()
            Me.Refresh()
        End If
    End Sub

    Private Sub Objects_Setting(ByVal sender As Object, ByVal e As CollectionEventArgs) Handles Objects.Setting
        If Not m_LockRepaint Then
            InvalidateCache()
            Me.Refresh()
        End If
    End Sub

#End Region

    Protected Overrides Sub OnMouseClick(ByVal e As System.Windows.Forms.MouseEventArgs)
        Dim x, y As Integer

        x = e.X - m_center.X
        y = e.Y - m_center.Y

        Dim ee As New MouseEventArgsExtended(e, x / m_pixelsInUnitX, -y / m_pixelsInUnitY)

        ' Режим редактирования
        If m_EditMode Then
            m_editedLineStrip.Add(New PointF(ee.tX, ee.tY))

            If Not m_editedLineStrip Is Nothing Then DrawObject(m_editedLineStrip)
        End If

        MyBase.OnMouseClick(ee)
    End Sub

    Protected Overrides Sub OnMouseMove(ByVal e As System.Windows.Forms.MouseEventArgs)
        Dim x, y As Integer

        x = e.X - m_center.X
        y = e.Y - m_center.Y

        Dim ee As New MouseEventArgsExtended(e, x / m_pixelsInUnitX, -y / m_pixelsInUnitY)
        MyBase.OnMouseMove(ee)
    End Sub

    Protected Overrides Sub OnMouseUp(ByVal e As System.Windows.Forms.MouseEventArgs)
        Dim x, y As Integer

        x = e.X - m_center.X
        y = e.Y - m_center.Y

        Dim ee As New MouseEventArgsExtended(e, x / m_pixelsInUnitX, -y / m_pixelsInUnitY)
        MyBase.OnMouseUp(ee)
    End Sub

    Protected Overrides Sub OnMouseDown(ByVal e As System.Windows.Forms.MouseEventArgs)
        Dim x, y As Integer

        x = e.X - m_center.X
        y = e.Y - m_center.Y

        Dim ee As New MouseEventArgsExtended(e, x / m_pixelsInUnitX, -y / m_pixelsInUnitY)
        MyBase.OnMouseDown(ee)
    End Sub

    Public Sub BeginEditMode(ByVal color As System.Drawing.Color)
        m_EditMode = True

        m_editedLineStrip = New LineStrip(color)
    End Sub

    Public Function EndEditMode() As LineStrip
        m_EditMode = False

        Dim m As LineStrip = m_editedLineStrip
        m_editedLineStrip = Nothing
        Return m
    End Function
End Class


