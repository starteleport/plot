﻿Public Class LineStrips
    Inherits System.Collections.CollectionBase

    Default Public Property Item(ByVal index As Integer) As LineStrip
        Get
            Return CType(List(index), LineStrip)
        End Get
        Set(ByVal value As LineStrip)
            List(index) = value
        End Set
    End Property

    Public Function Add(ByVal value As LineStrip) As Integer
        Return List.Add(value)
    End Function 'Add

    Public Function IndexOf(ByVal value As LineStrip) As Integer
        Return List.IndexOf(value)
    End Function 'IndexOf

    Public Sub Insert(ByVal index As Integer, ByVal value As LineStrip)
        List.Insert(index, value)
    End Sub 'Insert

    Public Sub Remove(ByVal value As LineStrip)
        List.Remove(value)
    End Sub 'Remove

    Public Function Contains(ByVal value As LineStrip) As Boolean
        ' If value is not of type Int16, this will return false.
        Return List.Contains(value)
    End Function 'Contains

    Public Event Inserting(ByVal sender As Object, ByVal e As CollectionEventArgs)
    Public Event Removing(ByVal sender As Object, ByVal e As CollectionEventArgs)
    Public Event Setting(ByVal sender As Object, ByVal e As CollectionEventArgs)
    Public Event Cleared(ByVal sender As Object, ByVal e As CollectionEventArgs)


    Protected Overrides Sub OnInsertComplete(ByVal index As Integer, ByVal value As Object)
        RaiseEvent Inserting(Me, New CollectionEventArgs(index, value))
    End Sub 'OnInsert

    Protected Overrides Sub OnRemoveComplete(ByVal index As Integer, ByVal value As Object)
        RaiseEvent Removing(Me, New CollectionEventArgs(index, value))
    End Sub 'OnRemove

    Protected Overrides Sub OnSetComplete(ByVal index As Integer, ByVal oldValue As Object, ByVal newValue As Object)
        RaiseEvent Setting(Me, New CollectionEventArgs(index, newValue, oldValue))
    End Sub 'OnSet

    Protected Overrides Sub OnValidate(ByVal value As Object)
        If Not GetType(LineStrip).IsAssignableFrom(value.GetType()) Then
            Throw New ArgumentException("value must be of type LineStrip.", "value")
        End If
    End Sub 'OnValidate 

    Protected Overrides Sub OnClearComplete()
        RaiseEvent Cleared(Me, New CollectionEventArgs)
    End Sub

End Class